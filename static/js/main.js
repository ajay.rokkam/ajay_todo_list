var todoList = JSON.parse(localStorage.getItem('items'));
var checkedList = JSON.parse(localStorage.getItem('check'));
//
var id1;
var text1;

window.onload = function() {
    if(todoList != null){
        displayList();
    }
    
};



function addItem() {
    if(todoList == null){
        todoList = [];
        checkedList = [];
    }
    var item = document.getElementById('todo-item');
    if(item.value != ''){
        todoList.push(item.value);
        checkedList.push(false);
        item.value='';
        addToLocalStorage();
        clearChanges();
        displayList();
    }
}

function addToLocalStorage() {
    localStorage.setItem('items', JSON.stringify(todoList));
    localStorage.setItem('check', JSON.stringify(checkedList));
    
}

function displayList() {
    var array = JSON.parse(localStorage.getItem('items'));
    var check = JSON.parse(localStorage.getItem('check'));
    var section = document.getElementById('todo');

    for(var i=0; i<array.length; i++){
        var division = document.createElement('li');
        division.id = i;
        division.draggable = 'true';
        division.className = 'list-item';
        division.addEventListener('dragstart', dragStart);
        division.addEventListener('dragover', dragOver);
        division.addEventListener('drop', dragDrop);
        var checkbox =document.createElement('input');
        checkbox.type = 'checkbox';
        checkbox.addEventListener('click', checkElement);
        checkbox.checked = check[i];
        var todoText = document.createElement('span');
        todoText.innerText = array[i];
        var cancel = document.createElement('div');
        cancel.className = 'cancel-img';
        cancel.addEventListener('click', removeElement);
        if(check[i]){
            todoText.style.textDecoration = 'line-through';
            todoText.style.color = 'gray';
            
        }
        division.appendChild(checkbox);
        division.appendChild(todoText);
        division.appendChild(cancel);
        section.appendChild(division);
    }

    
}

function clearChanges() {
    var ul = document.getElementById('todo');
   
    while(ul.firstChild){
        ul.removeChild(ul.firstChild);
    }


}

function checkElement(){
    if(this.checked == true){
        checkedList[this.parentNode.id] = true;
        addToLocalStorage();
        clearChanges();
        displayList();
    }
    else{
        checkedList[this.parentNode.id] = false;
        addToLocalStorage();
        clearChanges();
        displayList();
    }
    
}
function removeElement() {
    this.parentNode.parentNode.removeChild(this.parentNode);
    todoList.splice(this.parentNode.id, 1);
    checkedList.splice(this.parentNode.id, 1);
    addToLocalStorage();
}

function dragStart(e) {
    id1 = this.id;
    text1 = e.target.textContent;
}
function dragOver(e){
    e.preventDefault();

 

}
function dragDrop(e) {
    todoList[this.id] = text1;
    todoList[id1] = e.target.textContent;
    var c = checkedList[this.id];
    checkedList[this.id] = checkedList[id1];
    checkedList[id1] = c;
    
    addToLocalStorage();
    clearChanges();
    displayList();


}
function clickButton(event) {
    if (event.keyCode === 13) {
        document.getElementById("btn").click();
      }
    
}